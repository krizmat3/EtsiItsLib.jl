# ETSI-ITS Library for Julia
EtsiITSLib.jl is a Julia package that provides a set of functions transform
ETSI-ITS messages from `.json` format obtained by
[ETSI-ITS Parser](https://gitlab.fel.cvut.cz/krizmat3/etsi-its-parser)
to a more convenient format for further processing.

Most used output format is a `DataFrame` from `DataFrames.jl` package.

## Installation
```julia
using Pkg
Pkg.add("https://gitlab.fel.cvut.cz/krizmat3/EtsiItsLib.jl")
```

## Performance
The library is optimized for memory usage. You can load `.json` file of
any size, as long as you have enough memory to store the output
`DataFrame`.
