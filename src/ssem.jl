module Ssem
    export parse_json, write_csv, read_csv, session

    include("json_loader.jl");

    using DataFrames, Dates, CSV
    
    """
    Load SSEM messages from a file.

    Parameters
    ----------
    filename : String
        The name of the file to load the SSEM messages from.

    Returns
    -------
    DataFrame
        A DataFrame containing the SSEM messages.
    """
    function parse_json(filename)::DataFrames.DataFrame

        function isssem(x)
            return haskey(x, :timeStamp) && haskey(x, :fields) && haskey(x[:fields], :ssm)
        end

        file = open(filename, "r")

        df = DataFrames.DataFrame(
            timeStamp=Dates.DateTime[],
            stationID=Int[],
            intersectionID=Int[],
            requestID=Int[],
            requestStatus=Int[],
            inboundApproachID=Int[],
            outboundApproachID=Int[]
        )

        while true

            message = JsonLoader.json_next_msg!(file, "ssm");

            if isnothing(message)
                break;
            end

            if !isssem(message)
                continue;
            end


            timestamp_i = Dates.unix2datetime(message[:timeStamp])
            #station_id_i = message[:fields][:header][:stationID]

            for status in message[:fields][:ssm][:status]
                
                intersection_id_i = status[:id][:id]

                for sig_status in status[:sigStatus]

                    request_id_i = sig_status[:requester][:request]
                    requester_station_id_i = sig_status[:requester][:id][:stationID]
                    request_status_i = sig_status[:status]

                    inbound_id_i = sig_status[:inboundOn][:approach]
                    outbound_id_i = sig_status[:outboundOn][:approach]

                    push!(df, (
                        timestamp_i,
                        requester_station_id_i,
                        intersection_id_i,
                        request_id_i,
                        request_status_i,
                        inbound_id_i,
                        outbound_id_i
                    ))
                    
                end
            end
        end

        close(file)

        return df
    end

    function write_csv(
        filename,
        df::DataFrames.DataFrame;
    )
        df = copy(df)

        CSV.write(filename, df)
    end

    function read_csv(
        filename
    )::DataFrames.DataFrame

        df = CSV.read(filename, DataFrames.DataFrame)

        return df
    end

    function session(
        ssem::DataFrames.DataFrame;
        silence_threshold::Dates.TimePeriod = Dates.Second(30),
    )::DataFrames.DataFrame

        df = DataFrames.DataFrame(
            stationID=Int[],
            intersectionID=Int[],
            startTime=Dates.DateTime[],
            endTime=Dates.DateTime[],
            requestID=Int[],
            inboundApproachID=Int[],
            outboundApproachID=Int[]
        )

        for station in groupby(ssem, :stationID)
            station_id = station[1, :stationID]
            for intersection in groupby(station, :intersectionID)
                intersection_id = intersection[1, :intersectionID]
                for request in groupby(intersection, :requestID)
                    request_id = request[1, :requestID]
                    for inbound in groupby(request, :inboundApproachID)
                        inbound_id = inbound[1, :inboundApproachID]
                        for outbound in groupby(inbound, :outboundApproachID)
                            outbound_id = outbound[1, :outboundApproachID]

                            timediffs = diff(outbound.timeStamp)

                            splitpoints = [0, findall(x -> x > silence_threshold, timediffs)..., nrow(outbound)]
                
                            for i in filter(x -> x != 1, eachindex(splitpoints))
                
                                start_index = splitpoints[i-1] + 1
                                end_index = splitpoints[i]

                                push!(df, (
                                    station_id,
                                    intersection_id,
                                    outbound[start_index, :timeStamp],
                                    outbound[end_index, :timeStamp],
                                    request_id,
                                    inbound_id,
                                    outbound_id
                                ))
                            end
                        end
                    end

                end
            end
        end

        return df
    end

end
