module Mapem
    export parse_json, write_json, read_json, find_signal_groups, find_boundary

    include("json_loader.jl")

    using Geodesy, JSON3

    # Typical intersection dictionary looks like this
    #
    #   <intersection_id>:
    #       latitude: Float64
    #       longitude: Float64
    #       altitude: Float64
    #       lanes: {
    #           <lane_id>:
    #               laneType: Symbol
    #               nodes: [(Float64, Float64, Float64), ...]
    #               ingressApproach: Int
    #               egressApproach: Int
    #               connectsTo: {
    #                   <connecting_lane_id>: <signal_group_id>
    #                   ...
    #               }
    #           ...
    #     }

    """
    Load MAPEM messages from a file.

    Parameters
    ----------
    filename : String
        The name of the file to load the MAPEM and CAM messages from.

    Returns
    -------
    Dict
        A dictionary containing the MAPEM messages.
    """
    function parse_json(filename)::Dict{Int, Dict}
    
        is_message(x) = haskey(x, :timeStamp) && haskey(x, :fields)
        is_cam(x) = is_message(x) && haskey(x[:fields], :cam)
        is_mapem(x) = is_message(x) && haskey(x[:fields], :map)

        file = open(filename, "r")
    
        map_data = Dict{Int, Dict}()

        # registry of [<station_id> -> <altitude>]
        altitude_registry = Dict{Int, Float64}()

        # registry of [<station_id> -> <intersection_id>]
        station_id_registry = Dict{Int, Int}()

        while true
        
            message = JsonLoader.json_next_msg!(file)

            if isnothing(message)
                break
            end

            if is_cam(message)

                # is CAM

                station_id = message[:fields][:header][:stationID]
                altitude = message[:fields][:cam][:camParameters][:basicContainer][:referencePosition][:altitude][:altitudeValue]
                
                altitude_registry[station_id] = altitude

            elseif is_mapem(message)
            
                # is MAPEM
            
                station_id = message[:fields][:header][:stationID]

                if !haskey(message[:fields][:map], :intersections)
                    continue
                end

                for intersection in message[:fields][:map][:intersections]

                    intersection_id = Int(intersection[:id][:id])

                    station_id_registry[intersection_id] = station_id

                    # Add new intersection to the map data if it does not exist
                    if !haskey(map_data, intersection_id)
                        map_data[intersection_id] = Dict{Symbol, Any}()
                        map_data[intersection_id][:lanes] = Dict{Int, Dict}()
                    end

                    # Add the latitude, longitude, and altitude to the intersection
                    map_data[intersection_id][:latitude] = intersection[:refPoint][:lat]
                    map_data[intersection_id][:longitude] = intersection[:refPoint][:long]

                    # If the message has altitude data, use it, otherwise wait for later
                    if haskey(intersection[:refPoint], :elevation)
                        map_data[intersection_id][:altitude] = intersection[:refPoint][:elevation]
                    end

                    if !haskey(intersection, :laneSet)
                        continue
                    end

                    # Iterate over each lane in the intersection
                    for lane in intersection[:laneSet]
                        lane_id = Int(lane[:laneID])
                        
                        if !haskey(map_data[intersection_id][:lanes], lane_id)
                            
                            map_data[intersection_id][:lanes][lane_id] = Dict{Symbol, Any}()
                            map_data[intersection_id][:lanes][lane_id][:connectsTo] = Dict{Int, Int}()

                            lane_type = if haskey(lane[:laneAttributes][:laneType], :vehicle)
                                :vehicle
                            elseif haskey(lane[:laneAttributes][:laneType], :crosswalk)
                                :crosswalk
                            elseif haskey(lane[:laneAttributes][:laneType], :bikeLane)
                                :bikeLane
                            elseif haskey(lane[:laneAttributes][:laneType], :sidewalk)
                                :sidewalk
                            elseif haskey(lane[:laneAttributes][:laneType], :median)
                                :median
                            elseif haskey(lane[:laneAttributes][:laneType], :striping)
                                :striping
                            elseif haskey(lane[:laneAttributes][:laneType], :trackedVehicle)
                                :trackedVehicle
                            elseif haskey(lane[:laneAttributes][:laneType], :parking)
                                :parking
                            else
                                :other
                            end
                                

                            map_data[intersection_id][:lanes][lane_id][:type] = lane_type

                            nodes = Tuple{Float64, Float64}[]

                            cumulative_x_offset = 0.0
                            cumulative_y_offset = 0.0

                            for node in lane[:nodeList][:nodes]

                                x_offset::Int = 0
                                y_offset::Int = 0

                                if haskey(node[:delta], "node-XY1") 
                                    x_offset = node[:delta]["node-XY1"][:x]
                                    y_offset = node[:delta]["node-XY1"][:y]
                                elseif haskey(node[:delta], "node-XY2")
                                    x_offset = node[:delta]["node-XY2"][:x]
                                    y_offset = node[:delta]["node-XY2"][:y]
                                elseif haskey(node[:delta], "node-XY3")
                                    x_offset = node[:delta]["node-XY3"][:x]
                                    y_offset = node[:delta]["node-XY3"][:y]
                                elseif haskey(node[:delta], "node-XY4")
                                    x_offset = node[:delta]["node-XY4"][:x]
                                    y_offset = node[:delta]["node-XY4"][:y]
                                elseif haskey(node[:delta], "node-XY5")
                                    x_offset = node[:delta]["node-XY5"][:x]
                                    y_offset = node[:delta]["node-XY5"][:y]
                                elseif haskey(node[:delta], "node-XY6")
                                    x_offset = node[:delta]["node-XY6"][:x]
                                    y_offset = node[:delta]["node-XY6"][:y]
                                #elseif haskey(node[:delta], "node-LatLon")
                                    # TODO: convert lat/lon to x/y, or all other to lat/lon idk
                                end

                                cumulative_x_offset += float(x_offset) / 100.0
                                cumulative_y_offset += float(y_offset) / 100.0

                                push!(nodes, (cumulative_x_offset, cumulative_y_offset))

                            end

                            map_data[intersection_id][:lanes][lane_id][:nodes] = nodes

                            # Check for ingress and egress approaches

                            if haskey(lane, :ingressApproach)
                                map_data[intersection_id][:lanes][lane_id][:ingressApproach] = lane[:ingressApproach]
                            end

                            if haskey(lane, :egressApproach)
                                map_data[intersection_id][:lanes][lane_id][:egressApproach] = lane[:egressApproach]
                            end

                            if haskey(lane, :connectsTo)
                                for connection in lane[:connectsTo]
                                    connecting_lane = connection[:connectingLane][:lane]
                                    
                                    if haskey(connection, :signalGroup)
                                        signal_group = connection[:signalGroup]
                                        map_data[intersection_id][:lanes][lane_id][:connectsTo][connecting_lane] = signal_group
                                    else
                                        map_data[intersection_id][:lanes][lane_id][:connectsTo][connecting_lane] = nothing
                                    end
                                end
                            end
                        end
                    end
                end            
            end
        end

        to_delete = Set{Int}()

        # fill up altitudes
        for (intersection_id, intersection) in map_data
            if haskey(intersection, :altitude) || (haskey(station_id_registry, intersection_id) && haskey(altitude_registry, station_id_registry[intersection_id]))
                
                if !haskey(intersection, :altitude)
                    intersection[:altitude] = altitude_registry[station_id_registry[intersection_id]]
                end

                origin = Geodesy.LLA(
                    intersection[:latitude],
                    intersection[:longitude],
                    intersection[:altitude]
                )

                lla_from_enu = Geodesy.LLAfromENU(origin, Geodesy.wgs84)


                for lane in values(intersection[:lanes])
                 
                    nodes = []

                    for node in lane[:nodes]

                        x_offset, y_offset = node

                        as_enu = Geodesy.ENU(x_offset, y_offset, 0.0)
                        as_lla = lla_from_enu(as_enu)

                        push!(nodes, (as_lla.lat, as_lla.lon, as_lla.alt))
                    end

                    lane[:nodes] = nodes
                end
            else
                push!(to_delete, intersection_id)
            end
        end

        # list lanes as in approaches
        for intersection in values(map_data)

            ingress_approaches = Dict()
            egress_approaches = Dict()

            for (lane_id, lane) in intersection[:lanes]
                if haskey(lane, :ingressApproach)
                    
                    ingress_approach = lane[:ingressApproach]
                    if !haskey(ingress_approaches, ingress_approach)
                        ingress_approaches[ingress_approach] = []
                    end

                    push!(ingress_approaches[ingress_approach], lane_id)
                end

                if haskey(lane, :egressApproach)
                        
                    egress_approach = lane[:egressApproach]
                    if !haskey(egress_approaches, egress_approach)
                        egress_approaches[egress_approach] = []
                    end

                    push!(egress_approaches[egress_approach], lane_id)
                end
            end

            intersection[:ingressApproaches] = ingress_approaches
            intersection[:egressApproaches] = egress_approaches
        end

        # check if lanes are connected
        for (intersection_id, intersection) in map_data
            for lane in values(intersection[:lanes])
                for connecting_lane in keys(lane[:connectsTo])
                    if !haskey(map_data[intersection_id][:lanes], connecting_lane)
                        push!(to_delete, intersection_id)
                    end
                end
            end
        end

        for intersection_id in to_delete
            delete!(map_data, intersection_id)
        end

        close(file)

        return map_data
    end

    function write_json(
        filename,
        dict
    )
        JSON3.write(filename, dict)
    end

    function read_json(
        filename
    )
        return JSON3.read(filename)
    end

    function find_signal_groups(
        map_data,
        intersection_id,
        inbound_approach_id,
        outbound_approach_id
    )::Vector{Int}

        signal_groups = Set{Int}()

        intersection = map_data[intersection_id]

        inbound_lane_ids = intersection[:ingressApproaches][inbound_approach_id]
        outbound_lane_ids = intersection[:egressApproaches][outbound_approach_id]

        for inbound_lane_id in inbound_lane_ids
			inbound_lane = intersection[:lanes][inbound_lane_id]

			for outbound_lane_id in outbound_lane_ids
				if haskey(inbound_lane[:connectsTo], outbound_lane_id)
					push!(
						signal_groups,
						inbound_lane[:connectsTo][outbound_lane_id]
					)
				end
			end
		end
            
        return collect(signal_groups)
    end

    function find_boundary(
        map_data,
        intersection_id,
        inbound_approach_id,
    )::Vector{Vector{Float64}}

        boundary = Vector{Vector{Float64}}()

        intersection = map_data[intersection_id]

        inbound_lane_ids = intersection[:ingressApproaches][inbound_approach_id]

        for inbound_lane_id in inbound_lane_ids
            inbound_lane = intersection[:lanes][inbound_lane_id]

            push!(boundary, inbound_lane[:nodes][1])
        end

        return boundary
    end

end
