module Analysis
    export cross_time, distance_traveled

    using DataFrames, Dates, Geodesy, Interpolations

    """
    Calculate the distance between two points.

    Parameters
    ----------
    - p1 : ``AbstractVector{Float64, Float64, Float64}``
        The first point.
    - p2 : ``AbstractVector{Float64, Float64, Float64}``
        The second point.

    Returns
    -------
    ``Float64``
        The distance between the two points.

    Examples
    --------

    ```julia
    p1 = [0, 0, 0]
    p2 = [10, 0, 0]

    distance(p1, p2)
    ```
    """
    function distance(
        p1::AbstractVector{Float64},
        p2::AbstractVector{Float64};
    )

        lat1, lon1, alt1 = p1
        lat2, lon2, alt2 = p2

        alt = (alt1 + alt2) / 2

        q1 = Geodesy.LLA(lat1, lon1, alt)
        q2 = Geodesy.LLA(lat2, lon2, alt)

        return Geodesy.euclidean_distance(q1, q2)
    end

    """
    Calculate the time at which a vehicle crosses a given point.

    Parameters
    ----------
    - time : ``AbstractVector{Dates.DateTime}``
        The time of each position measurement.
    - position : ``AbstractVector{AbstractVector{Float64, Float64, Float64}}``
        The position of the vehicle at each time.
    - cross_points : ``AbstractVector{AbstractVector{Float64, Float64, Float64}}``
        The points at which the vehicle crosses.

    Returns
    -------
    ``Dates.DateTime``
        The time at which the vehicle crosses the given point.

    Examples
    --------

    ```julia
    time = [
        Dates.DateTime(2021, 1, 1, 0, 0, 0),
        Dates.DateTime(2021, 1, 1, 0, 0, 1),
        Dates.DateTime(2021, 1, 1, 0, 0, 2),
        Dates.DateTime(2021, 1, 1, 0, 0, 3),
    ]
    position = [
        [0, 0, 0],
        [10, 0, 0],
        [20, 0, 0],
        [30, 0, 0],
    ]
    cross_points = [
        [0, 0, 0],
        [0, 10, 0],
        [0, 20, 0],
        [0, 30, 0],
    ]

    cross_time(time, position, cross_points)
    ```
    """
    function cross_time(
        time::Vector{Dates.DateTime},
        position::Vector{Vector{Float64}},
        cross_points::Vector{Vector{Float64}};
        samples::Int = 10_000,
    )::Dates.DateTime

        x = range(
            datetime2unix(minimum(time)),
            datetime2unix(maximum(time)),
            samples,
        )

        t = datetime2unix.(time)
        Interpolations.deduplicate_knots!(t)
        interp = Interpolations.linear_interpolation(t, position)

        y = interp.(x)

        min_timeindex = []
        min_distances = []
        for cross_point in cross_points
            cross_point_distance(x) = distance(x, cross_point)
            d = cross_point_distance.(y)
            push!(min_timeindex, argmin(d))
            push!(min_distances, minimum(d))
        end

        return unix2datetime(x[min_timeindex[argmin(min_distances)]])
    end

    """
    Calculate the distance traveled by a vehicle given the time and speed.

    Parameters
    ----------
    - time : ``AbstractVector{Dates.DateTime}``
        The time of each speed measurement.
    - speed : ``AbstractVector{Float64}``
        The speed of the vehicle at each time.

    Returns
    -------
    ``AbstractVector{Float64}``
        The distance traveled by the vehicle at each time.

    Examples
    --------

    ```julia
    time = [
        Dates.DateTime(2021, 1, 1, 0, 0, 0),
        Dates.DateTime(2021, 1, 1, 0, 0, 1),
        Dates.DateTime(2021, 1, 1, 0, 0, 2),
        Dates.DateTime(2021, 1, 1, 0, 0, 3),
    ]
    speed = [0, 10, 20, 30]

    distance_traveled(time, speed)
    ```
    """
    function distance_traveled(
        time::Vector{Dates.DateTime},
        speed::Vector{Float64},
    )

        t = datetime2unix.(time)
        dt = diff(t)

        s = (speed[1:end-1] + speed[2:end]) ./ 2 .* dt

        return [0, s...]
    end

end
