module Cam
    export StationType, parse_json, write_csv, read_csv, station_info, session

    include("json_loader.jl");

    using DataFrames, Dates, LeapSeconds, CSV

    @enum StationType begin
        unknown = 0
        pedestrian = 1
        cyclist = 2
        moped = 3
        motorcycle = 4
        passengerCar = 5
        bus = 6
        lightTruck = 7
        heavyTruck = 8
        trailer = 9
        specialVehicles = 10
        tram = 11
        roadSideUnit = 15
    end

    """
    Load CAM messages from a file.

    Parameters
    ----------
    filename : String
        The name of the file to load the CAM messages from.

    Returns
    -------
    DataFrame
        A DataFrame containing the CAM messages.
    """
    function parse_json(filename)::DataFrames.DataFrame
        
        function iscam(x)
            return haskey(x, :timeStamp) && haskey(x, :fields) && haskey(x[:fields], :cam);
        end

        function generationDeltaTime2datetime(gen_delta_time::Number, time_stamp::Dates.DateTime)

            origin = Dates.DateTime(2004, 1, 1, 0, 0, 0);
        
            origin_offset = LeapSeconds.offset_utc_tai(origin)
            time_stamp_offset = LeapSeconds.offset_utc_tai(time_stamp)
        
            offset_diff = time_stamp_offset - origin_offset
        
            mod = 65_536;
        
            diff_time_stamp = Dates.Millisecond(time_stamp - origin).value
        
            base_mid = Int(round(diff_time_stamp / mod))
            base_upp = base_mid + 1
            base_low = base_mid - 1
        
            if offset_diff > 0
                origin += Dates.Second(abs(offset_diff))
            elseif offset_diff < 0
                origin -= Dates.Second(abs(offset_diff))
            end
        
            upp = origin + Dates.Millisecond(base_upp * mod + gen_delta_time)
            mid = origin + Dates.Millisecond(base_mid * mod + gen_delta_time)
            low = origin + Dates.Millisecond(base_low * mod + gen_delta_time)
        
            diff_upp = abs(time_stamp - upp)
            diff_mid = abs(time_stamp - mid)
            diff_low = abs(time_stamp - low)
        
            values = [upp, mid, low]
            diffs = [diff_upp, diff_mid, diff_low]
        
            return values[argmin(diffs)]
        end

        file = open(filename, "r")

        df = DataFrames.DataFrame(
            timeStamp=Dates.DateTime[],
            stationID=Int[],
            stationType=StationType[],
            latitude=Float64[],
            longitude=Float64[],
            altitude=Float64[],
            heading=Union{DataFrames.Missing, Float64}[],
            speed=Union{DataFrames.Missing, Float64}[],
            length=Union{DataFrames.Missing, Float64}[],
            width=Union{DataFrames.Missing, Float64}[],
        )

        # currently we do not care about low frequency or special container

        while true
            message = JsonLoader.json_next_msg!(file, "cam")

            if isnothing(message)
                break
            end

            if !iscam(message)
                continue
            end

            vehicle_heading = missing
            vehicle_speed = missing
            vehicle_length = missing
            vehicle_width = missing

            if haskey(message[:fields][:cam][:camParameters][:highFrequencyContainer], :basicVehicleContainerHighFrequency)
                highFrequencyContainer = message[:fields][:cam][:camParameters][:highFrequencyContainer]
                vehicleHighFrequency = highFrequencyContainer[:basicVehicleContainerHighFrequency]

                vehicle_heading = vehicleHighFrequency[:heading][:headingValue]
                vehicle_speed = vehicleHighFrequency[:speed][:speedValue]
                vehicle_length = vehicleHighFrequency[:vehicleLength][:vehicleLengthValue]
                vehicle_width = vehicleHighFrequency[:vehicleWidth]
            end

            push!(df, (
                generationDeltaTime2datetime(message[:fields][:cam][:generationDeltaTime], Dates.unix2datetime(message[:timeStamp])),
                message[:fields][:header][:stationID],
                StationType(message[:fields][:cam][:camParameters][:basicContainer][:stationType]),
                message[:fields][:cam][:camParameters][:basicContainer][:referencePosition][:latitude],
                message[:fields][:cam][:camParameters][:basicContainer][:referencePosition][:longitude],
                message[:fields][:cam][:camParameters][:basicContainer][:referencePosition][:altitude][:altitudeValue],
                vehicle_heading,
                vehicle_speed,
                vehicle_length,
                vehicle_width,
            ))
        end

        close(file)

        return df
    end

    function write_csv(
        filename,
        df::DataFrames.DataFrame;
    )
        
        df = copy(df)

        if :stationType in names(df) .|> Symbol
            df.stationType = map(Int, df.stationType)
        end

        CSV.write(filename, df)
    end

    function read_csv(
        filename
    )::DataFrames.DataFrame

        df = CSV.read(filename, DataFrames.DataFrame)

        if :stationType in names(df) .|> Symbol
            df.stationType = map(StationType, df.stationType)
        end

        return df
    end
    

    """
    Extract station information from CAM messages.

    Parameters
    ----------
    cam : DataFrame
        A DataFrame containing CAM messages.

    Returns
    -------
    DataFrame
        A DataFrame containing the station information.
    """
    function station_info(
        cam::DataFrames.DataFrame,
    )::DataFrames.DataFrame
    
        station_info = Dict{Int, NamedTuple}();

        for message in eachrow(cam)
            station_id = message[:stationID]
            station_type = message[:stationType]

            if !haskey(station_info, station_id)
                station_info[station_id] = (
                    stationType=station_type,
                )
            end
        end

        df = DataFrames.DataFrame(
            stationID=Int[],
            stationType=StationType[],
        );

        for (station_id, info) in station_info
            push!(df, (
                station_id,
                info.stationType,
                ))
        end

        return df;
    end

    function session(
        cam::DataFrames.DataFrame;
        silence_threshold::Dates.TimePeriod = Dates.Second(10),
    )::DataFrames.DataFrame

        df = DataFrames.DataFrame(
            stationID=Int[],
            stationType=StationType[],
            startTime=Dates.DateTime[],
            endTime=Dates.DateTime[],
        )

        for station in groupby(cam, :stationID)

            station_id = station[1, :stationID]
            cam_station = filter(row -> row.stationID == station_id, cam)

            timediffs = diff(cam_station.timeStamp)

            splitpoints = [0, findall(x -> x > silence_threshold, timediffs)..., nrow(cam_station)]

            for i in filter(x -> x != 1, eachindex(splitpoints))

                start_index = splitpoints[i-1] + 1
                end_index = splitpoints[i]
            
                push!(df, (
                    station_id,
                    cam_station[start_index, :stationType],
                    cam_station[start_index, :timeStamp],
                    cam_station[end_index, :timeStamp],
                ))
            
            end
        end

        return df
    end
   
end
