module Spatem
    export EventState, parse_json, write_csv, read_csv, event_states

    include("timeutils.jl");
    include("json_loader.jl");

    using DataFrames, Dates, CSV

    @enum EventState begin
        unavailable = 0
        dark = 1
        stopThenProceed = 2
        stopAndRemain = 3
        preMovement = 4
        permissiveMovementAllowed = 5
        protectedMovementAllowed = 6
        permissiveClearence = 7
        protectedClearence = 8
        cautionConflictingTraffic = 9
    end

    """
    Load SPATEM messages from a file.

    Parameters
    ----------
    filename : String
        The name of the file to load the SPATEM messages from.

    Returns
    -------
    DataFrame
        A DataFrame containing the SPATEM messages.
    """
    function parse_json(filename)::DataFrames.DataFrame

        probability_conversion = Dict([
            0 => 0.21,
            1 => 0.36,
            2 => 0.47,
            3 => 0.56,
            4 => 0.62,
            5 => 0.68,
            6 => 0.73,
            7 => 0.77,
            8 => 0.81,
            9 => 0.85,
            10 => 0.88,
            11 => 0.91,
            12 => 0.94,
            13 => 0.96,
            14 => 0.98,
            15 => 1.0
        ])

        function isspatem(x)
            return haskey(x, :timeStamp) && haskey(x, :fields) && haskey(x[:fields], :spat);
        end

        file = open(filename, "r");

        df = DataFrames.DataFrame(
            timeStamp=Dates.DateTime[],
            stationID=Int[],
            intersectionID=Int[],
            numberOfSignalGroups=Int[],
            signalGroupID=Int[],
            eventState=EventState[],
            minEndTime=Dates.DateTime[],
            startTime=Dates.DateTime[],
            maxEndTime=Dates.DateTime[],
            likelyTime=Dates.DateTime[],
            confidence=Float64[]
        );

        while true
            message = JsonLoader.json_next_msg!(file, "spat");

            if isnothing(message)
                break;
            end

            if !isspatem(message)
                continue;
            end

            timestamp_i = Dates.unix2datetime(message[:timeStamp]);
            station_id_i = message[:fields][:header][:stationID];

            for intersection in message[:fields][:spat][:intersections]
                intersection_id_i = intersection[:id][:id];
                intersection_time_stamp_i = TimeUtils.moy_and_timestamp_to_datetime(intersection[:moy], intersection[:timeStamp], Dates.year(timestamp_i));
                intersection_number_of_signal_groups_i = length(intersection[:states]);

                for state in intersection[:states]
                    signal_group_id_i = state[:signalGroup];
                    event_state_i = state["state-time-speed"][1][:eventState];

                    
                    min_end_time_i = TimeUtils.offset_to_datetime(state["state-time-speed"][1][:timing][:minEndTime], intersection_time_stamp_i);
                    start_time_i = TimeUtils.offset_to_datetime(state["state-time-speed"][1][:timing][:startTime], intersection_time_stamp_i);
                    max_end_time_i = TimeUtils.offset_to_datetime(state["state-time-speed"][1][:timing][:maxEndTime], intersection_time_stamp_i);
                    likely_time_i = TimeUtils.offset_to_datetime(state["state-time-speed"][1][:timing][:likelyTime], intersection_time_stamp_i);
                    confidence_i = state["state-time-speed"][1][:timing][:confidence];

                    # replace with push
                    push!(df, (
                        intersection_time_stamp_i,
                        station_id_i,
                        intersection_id_i,
                        intersection_number_of_signal_groups_i,
                        signal_group_id_i,
                        EventState(event_state_i),
                        min_end_time_i,
                        start_time_i,
                        max_end_time_i,
                        likely_time_i,
                        probability_conversion[confidence_i]
                    ));

                end
            end

        end

        close(file);

        return df;
    end

    function write_csv(
        filename,
        df::DataFrames.DataFrame;
    )

        df = copy(df)

        if :eventState in names(df) .|> Symbol
            df.eventState = map(Int, df.eventState)
        end

        CSV.write(filename, df)
    end

    function read_csv(
        filename
    )::DataFrames.DataFrame

        df = CSV.read(filename, DataFrames.DataFrame)
        
        if :eventState in names(df) .|> Symbol
            df.eventState = map(EventState, df.eventState)
        end

        return df
    end

    function event_states(
        spatem::DataFrames.DataFrame;
    )::DataFrames.DataFrame

        df = DataFrames.DataFrame(
            intersectionID=Int[],
            signalGroupID=Int[],
            eventState=EventState[],
            startTime=Dates.DateTime[],
            endTime=Dates.DateTime[],
            duration=Float64[],
        )

        # intersectionID -> signalGroupID -> value
        current_event_state = Dict{Int, Dict{Int, EventState}}()
        current_start_time = Dict{Int, Dict{Int, DateTime}}()
        last_start_time = Dict{Int, Dict{Int, DateTime}}()

        for intersection_id in unique(spatem.intersectionID)
            current_event_state[intersection_id] = Dict{Int, EventState}()
            current_start_time[intersection_id] = Dict{Int, DateTime}()
            last_start_time[intersection_id] = Dict{Int, DateTime}()
        end

        for row in reverse(eachrow(spatem))
            
            intersection_id = row.intersectionID
            signal_group_id = row.signalGroupID

            if !haskey(current_event_state[intersection_id], signal_group_id)
                current_event_state[intersection_id][signal_group_id] = row.eventState
            elseif current_event_state[intersection_id][signal_group_id] != row.eventState

                if haskey(last_start_time[intersection_id], signal_group_id)
                    push!(df, (
                        intersectionID=intersection_id,
                        signalGroupID=signal_group_id,
                        eventState=current_event_state[intersection_id][signal_group_id],
                        startTime=current_start_time[intersection_id][signal_group_id],
                        endTime=last_start_time[intersection_id][signal_group_id],
                        duration=Millisecond(last_start_time[intersection_id][signal_group_id] - current_start_time[intersection_id][signal_group_id]).value / 1000
                    ))
                end

                last_start_time[intersection_id][signal_group_id] = current_start_time[intersection_id][signal_group_id]
            end


            current_event_state[intersection_id][signal_group_id] = row.eventState
            current_start_time[intersection_id][signal_group_id] = row.startTime
        end

        return df
    end 



end
