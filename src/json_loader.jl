module JsonLoader
    export json_next_msg!

    using JSON3
   
    """
    Helper function to read the next JSON message from a file without
    loading the whole file into memory. The function will return the
    next JSON message as a Dict if the message contains the string
    `check`. If `check` is not provided, the function will return the
    next JSON message.
    """
    function json_next_msg!(file, check = nothing)

        buffer = IOBuffer();

        readuntil(file, '{');
        level = 1;

        write(buffer, '{');

        while !eof(file)
        
            chunk = readuntil(file, '}', keep=true);

            level += count(ch -> ch == '{', chunk);
            level -= count(ch -> ch == '}', chunk);
        
            write(buffer, chunk);

            if level == 0
                as_string = String(take!(buffer));

                if isnothing(check) || occursin(check, as_string)
                    return JSON3.read(as_string);
                else
                    buffer = IOBuffer();
                    readuntil(file, '{');
                    level = 1;
                    write(buffer, '{');
                end
            end
        end

        return nothing;
    end

end
