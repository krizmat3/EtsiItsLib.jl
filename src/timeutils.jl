module TimeUtils
    export moy_and_timestamp_to_datetime, offset_to_datetime

    using Dates

    function moy_and_timestamp_to_datetime(moy::Int, time_stamp::Int, year::Int)::Dates.DateTime

        msoy = Float64(moy)*60.0*1000.0 + Float64(time_stamp);

        return Dates.DateTime(year, 1, 1, 0, 0, 0, 0) + Dates.Millisecond(msoy);
    end

    function offset_to_datetime(offset::Int, datetime::Dates.DateTime)::Dates.DateTime

        if offset == 36001
            return datetime
        end

        # datetime floored to hour
        floor_datetime = Dates.DateTime(year(datetime), month(datetime), day(datetime), hour(datetime), 0, 0, 0);

        upper_datetime = floor_datetime + Dates.Hour(1) + Dates.Millisecond(offset*100);
        lower_datetime = floor_datetime + Dates.Millisecond(offset*100);


        if abs(datetime2unix(lower_datetime) - datetime2unix(datetime)) <
            abs(datetime2unix(upper_datetime) - datetime2unix(datetime))
            return lower_datetime
        else
            return upper_datetime
        end 
    end
end
