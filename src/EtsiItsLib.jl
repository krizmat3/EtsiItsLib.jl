module EtsiItsLib
    export Cam, Spatem, Mapem, Ssem, Analysis

    # Main modules for the message types
    include("cam.jl")
    include("spatem.jl")
    include("mapem.jl")
    include("ssem.jl")

    include("analysis.jl")

end # module
